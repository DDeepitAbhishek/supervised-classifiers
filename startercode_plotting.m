%code for Linear Discriminate using the least squares
%pattern recognition EE552
%Deepit Abhishek Durairaj, Feb 2018
% purpose
% Finding the linear discriminant using least squares for training and test data
% Discriminant function to classify predicted data(one vs all)

%///////////////////////////////////////////////////////////////
%//Output test accuracy -                                     //
%//1. wine -range(90%-94%)                                    //                                     //
%///////////////////////////////////////////////////////////////

%% Load the data
% Choose which dataset to use (choices wine, wallpaper, taiji) :
dataset = 'wine';
[train_featureVector, train_labels, test_featureVector, test_labels] = loadDataset(dataset);

%preserving the original data sets
train_featureVector_original = train_featureVector;
test_featureVector_original = test_featureVector;
train_labels_original = train_labels;
test_labels_original = test_labels;

%getting the class labels
test_labels = nominal(test_labels);
o = getlevels(test_labels);
class_number = grp2idx(o);

%getting the number classes
numGroups = length(countcats(test_labels));

%% organising the data

%get two features of the training and test data
featureA = 1;
featureB = 7;
feature_idx = [featureA,featureB];

train_featureVector = train_featureVector(:,feature_idx);
test_featureVector = test_featureVector(:,feature_idx);

%inserting the a column of 1 for finding bias coefficients
A = ones(size(train_featureVector(:,1)));
train_featureVector = [A train_featureVector];

%inserting the a column of 1 for finding bias coefficients
B = ones(size(test_featureVector(:,1)));
test_featureVector = [B test_featureVector];


%% training the discriminator and predicting the test labels

%getting the weighted coefficients
W = coefficients(train_featureVector,train_labels,test_labels);

% predicting the training labels
train_pred = leastSquares(W,train_featureVector,test_labels);
train_pred = categorical(transpose(train_pred));

%predicting the test labels
test_pred = leastSquares(W,test_featureVector,test_labels);
test_pred = categorical(transpose(test_pred));
%% Confusion matrix and Classification matrix
train_ConfMat = confusionmat(train_labels_original,train_pred)
% Create classification matrix (rows should sum to 1)
train_ClassMat = train_ConfMat./(meshgrid(countcats(train_labels_original))')
% mean group accuracy and std
train_acc = mean(diag(train_ClassMat))
train_std = std(diag(train_ClassMat))

test_ConfMat = confusionmat(test_labels_original,test_pred)
% Create classification matrix (rows should sum to 1)
test_ClassMat = test_ConfMat./(meshgrid(countcats(test_labels_original))')
% mean group accuracy and std
test_acc = mean(diag(test_ClassMat))
test_std = std(diag(test_ClassMat))
%% Get coefficients for plotting
mdllinear = plotting_coefficients(W,numGroups);

%%  Display the linear discriminants and a set of features in two of the feature dimensions

figure(1)
visualizeBoundaries(mdllinear,test_featureVector_original,test_labels_original,1,7)
title('{\bf Linear Discriminant Classification}')








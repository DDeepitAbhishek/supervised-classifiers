%project 2 - Fischer Projection
%pattern Recognition EE552
%Deepit Abhishek Durairaj, Feb 2018
% purpose
% Finding the fischer projection for training and test data
% Discriminant function to classify the fischer projected data

%///////////////////////////////////////////////////////////////
%//Output test accuracy -                                     //
%//1. wine - range(95%-99%)                                   //
%//2. wallpaper- 70.59%                                       //
%//3. Taiji -    95.73%                                       //
%///////////////////////////////////////////////////////////////

%% Load the data
% Choose which dataset to use (choices wine, wallpaper, taiji) :
dataset = input('enter dataset in quotes-');
[train_featureVector, train_labels, test_featureVector, test_labels] = loadDataset(dataset);

%store the original train and test labels
train_labels_original = train_labels;
test_labels_original = test_labels;

%get the number of classes
numGroups = length(countcats(test_labels));
%%  Organizing the training and test data

%get all the features of the training and test data
feature_idx = 1:size(train_featureVector,2);
train_featureVector = train_featureVector(:,feature_idx);
test_featureVector = test_featureVector(:,feature_idx);

%% Fisher Projection
% obtaining fischer coefficients 
w_fischer = fischer_projection(train_featureVector,train_labels);

% obtaining fischer projection for training data and test data
train_projected = train_featureVector * w_fischer;
test_projected = test_featureVector * w_fischer;
%% Fischer Discriminant by KNN

%KNN classifier for the training data
[closest_neighbours distance] = fischer_discriminant(train_projected,train_projected,10);
closest_neighbours = mapping_to_classes_fischer(closest_neighbours,train_labels_original);
fischer_train_labels = categorical(transpose(closest_neighbours));

%KNN classifier for the test data
[closest_neighbours_test distance_test] = fischer_discriminant(train_projected,test_projected,10);
closest_neighbours_test = mapping_to_classes_fischer(closest_neighbours_test,train_labels_original);
fischer_test_labels = categorical(transpose(closest_neighbours_test)); 

%% Confusion matrix and classification matrix

train_ConfMat = confusionmat(train_labels_original,fischer_train_labels)
% Create classification matrix (rows should sum to 1)
train_ClassMat = train_ConfMat./(meshgrid(countcats(train_labels_original))')
% mean group accuracy and std
train_acc = mean(diag(train_ClassMat))
train_std = std(diag(train_ClassMat))

% Create confusion matrix
 test_ConfMat = confusionmat(test_labels_original,fischer_test_labels)
% Create classification matrix (rows should sum to 1)
 test_ClassMat = test_ConfMat./(meshgrid(countcats(test_labels_original))')
% mean group accuracy and std
 test_acc = mean(diag(test_ClassMat))
 test_std = std(diag(test_ClassMat))

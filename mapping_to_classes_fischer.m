function f = mapping_to_classes_fischer(closest_neighbours,train_labels)
s = size(closest_neighbours);
for i = 1 : s(1)
    for p = 1 : s(2)
        r(i,p) = train_labels(closest_neighbours(i,p));
    end
end

for i = 1 :length(r)
    f(i) = mode(r(i,:),2);
end



function  mdllinear = plotting_coefficients(W,numGroups) 
mdllinear = {};
mdllinear.Coeffs.Const =[];
mdllinear.Coeffs.Linear =[];
for i = 1:numGroups
    for j = 1:numGroups
        if i~=j
            k=1;
            mdllinear.Coeffs(i,j).Const = W(k,i) - W(k,j);
            mdllinear.Coeffs(i,j).Linear = [W(2,i) - W(2,j);W(3,i) - W(3,j)];
        end
    end
end
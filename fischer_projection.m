%function for getting the fischer coefficients
%input - train_featureVector
%        train_labels
%output - w_fischer
function w_fischer = fischer_projection(train_featureVector,train_labels)
s_w = 0; 
s_b = 0;
train_labels = nominal(train_labels);
%get the training labels
o = getlevels(train_labels);
%get the number of groups
numGroups = length(countcats(train_labels));
feature_idx = 1:size(train_featureVector,2);
size_of_eachClass = zeros(numGroups,1);
%initialize a matrix for getting mean of each  class
m_k = cell(numGroups,1);
%mean of the whole data
m = mean(train_featureVector);
dummy_cell = cell(numGroups,1);
%obtaining the s_b and s_w
for i = 1:numGroups
     featureVector_eachClass = train_featureVector(train_labels == o(i),:);
     size_of_eachClass(i) = size(featureVector_eachClass,1);
     m_k{i} = mean(featureVector_eachClass);
     m_k_new = repmat(m_k{i},size_of_eachClass(i),1);
     featureVector_covariance = featureVector_eachClass - m_k_new;
     dummy_cell{i} = 0;
     for j = 1:size(feature_idx,2)
         for p = j:size(feature_idx,2)
             dummy_cell{i}(j,p) = transpose(featureVector_covariance(:,j)) * featureVector_covariance(:,p);
             dummy_cell{i}(p,j) = dummy_cell{i}(j,p);
         end
     end
     s_w = s_w + dummy_cell{i};
     s_b = s_b + size_of_eachClass(i) * transpose((m_k{i} - m)) * (m_k{i} - m);
     sw_inv_sb = (s_w)\s_b;
end
%evaluation the eigen vectors and eigen values
[eigen_vector , eigen_values] = eig(sw_inv_sb);
eigen_values = diag(eigen_values);

%sorting the eigen values in the descending order
[eigen_values , rank] = sort(eigen_values,'descend');
w_fischer = [];
%sorting the eigen vectors in the order of eigen values
%the sorted eigen vectors are the new weighted coefficients for fischer
%projection.
for j = 1:numGroups - 1
   w_fischer = [w_fischer eigen_vector(:,rank(j))];
end
end

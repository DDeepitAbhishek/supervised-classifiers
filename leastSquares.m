function f = leastSquares(W,train_featureVector,test_labels)
numGroups = length(countcats(test_labels));
test_labels = nominal(test_labels);
o = getlevels(test_labels);
class_number = grp2idx(o);

predicted_labels = train_featureVector * W;

for i = 1 :length(predicted_labels)
    max1 = 0;
    index = 0;
    for j = 1 : numGroups
        if predicted_labels(i,j)> max1
        max1 = predicted_labels(i,j);
        index = j;
        end
    end
    m(i)= index;
end

for i = 1 : length(predicted_labels)
    for j = 1 : numGroups
        if m(i)== class_number(j)
            f(i) = string(o(class_number(j)));
        end
    end
end


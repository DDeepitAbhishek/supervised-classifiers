%function to find the neighbours with least distances(KNN)
%input - train_featureVector,test_featureVector
%        k (number of closest to neighbours to check)
%output - closest_neighbours - gets the least distant K neighbours
%         neighbourDistances - corresponding distance of each neighbour
function [closest_neighbours, neighborDistances] = fischer_discriminant(train_featureVector,test_featureVector,k)
closest_neighbours = zeros(length(test_featureVector),k);
neighborDistances = zeros(length(test_featureVector),k);

for i=1:length(test_featureVector)
    %finding the distance between two data points
    distance_between_data = sum((repmat(test_featureVector(i,:),length(train_featureVector),1)-train_featureVector).^2,2);
    %sorting the distance in ascending order
    [distances ,neighbours] = sort(distance_between_data,'ascend');
    %getting the K least distances
    closest_neighbours(i,:) = neighbours(1:k);
    %getting the corresponding data points
    neighborDistances(i,:) = sqrt(distances(1:k));
end
end
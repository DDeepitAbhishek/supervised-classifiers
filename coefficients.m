
function r = coefficients(train_featureVector,train_labels,test_labels) 
numGroups = length(countcats(test_labels));
test_labels = nominal(test_labels);
o = getlevels(test_labels);
class_number = grp2idx(o);

new_train_labels = zeros(length(train_featureVector),numGroups);
for i = 1 : size(train_labels)
    for p =1:numGroups
        if train_labels(i) == string(o(p))
            new_train_labels(i)=p;
        end
    end
end

for i = 1 : size(train_labels)
    newer_train_labels(i,new_train_labels(i))=1;
end

r = (transpose(train_featureVector)*train_featureVector)\(transpose(train_featureVector)*newer_train_labels);



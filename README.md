main codes-	startercode_leastsquares.m - main function to execute least square method with one vs all classifier
		startercode_fischer.m      - main function to execute Fischer criterion with KNN classifier
		startercode_plotting.m     - main function to plot the data for wine dataset
		extra_credit.m             - main function to check relation of Fischer Criterion to least squares

supported functions- coefficients.m - To find W for least square
                     leastSquares.m - To predict the classes for the given data using one vs all
                     fischer_discriminant - To find the K nearest neighbours using KNN
                     fischer_projection - To project the training data to reduced dimensions and getting corresponding W
                     mapping_to_classes_fischer - to predict the classes for the given data using KNN
                     plotting_coefficients - To get the parameters used to plotting function visualizeBoundaries  
                         

Code to plot-visualizeBoundaries
             visualizeBoundariesFill




#################################
1. Run startercode_leastsquares.m to get the results for least square method.It will ask prompt to enter dataset. Enter the 'wine' or 'wallpaper' or 'taiji' in quotes.

2. Run startercode_fischer.m to get the results for fischer method.It will ask prompt to enter dataset. Enter the 'wine' or 'wallpaper' or 'taiji' in quotes.

3. Run startercode_plotting.m to get the plot for the wine data.

4. Run extra_credit.m to get the verify fischer with least squares.

NOTE - 
startercode_leastsquares and startercode_fischer will give prompt for entering the dataset. 
Enter desired dataset in ''.
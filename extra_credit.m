%code for Linear Discriminate using the least squares
%pattern recognition EE552
%Deepit Abhishek Durairaj, Feb 2018
% purpose
% Finding the linear discriminant using least squares for training and test data
% Discriminant function to classify predicted data(one vs all)

%///////////////////////////////////////////////////////////////
%//Output test accuracy -                                     //
%//1. wine -range(90%-94%)                                    //                                     //
%///////////////////////////////////////////////////////////////

%% Load the data
% Choose which dataset to use (choices wine, wallpaper, taiji) :
dataset = 'wine';
[train_featureVector, train_labels, test_featureVector, test_labels] = loadDataset(dataset);

%preserving the original data sets
train_featureVector_original = train_featureVector;
test_featureVector_original = test_featureVector;
train_labels_original = train_labels;
test_labels_original = test_labels;

%data for extra credit
train_featureVector_extra = train_featureVector(1:66,:);
test_featureVector_extra = test_featureVector(1:64,:);
train_labels_extra = train_labels(1:66,:);
test_labels_extra = test_labels(1:64,:);

train_featureVector_extra_original = train_featureVector(1:66,:);
test_featureVector_extra_original = test_featureVector(1:64,:);
train_labels_extra_original = train_labels_extra(1:66,:);
test_labels_extra_original = test_labels_extra(1:64,:);

%getting the class labels
test_labels_extra = nominal(test_labels_extra);
o = getlevels(test_labels_extra);
class_number = grp2idx(o);

%getting the number classes
numGroups = length(countcats(test_labels_extra));

%% organising the data

%get two features of the training and test data
feature_idx = 1:size(train_featureVector,2);
train_featureVector_extra = train_featureVector_extra(:,feature_idx);
test_featureVector_extra = test_featureVector_extra(:,feature_idx);

%inserting the a column of 1 for finding bias coefficients
A = ones(size(train_featureVector_extra(:,1)));
train_featureVector_extra = [A train_featureVector_extra];

%inserting the a column of 1 for finding bias coefficients
B = ones(size(test_featureVector_extra(:,1)));
test_featureVector_extra = [B test_featureVector_extra];


%% training the discriminator and predicting the test labels

%getting the weighted coefficients
W = coefficients(train_featureVector_extra,train_labels_extra,test_labels_extra);

% predicting the training labels
train_pred = leastSquares(W,train_featureVector_extra,test_labels_extra);
train_pred = categorical(transpose(train_pred));

%predicting the test labels
test_pred = leastSquares(W,test_featureVector_extra,test_labels_extra);
test_pred = categorical(transpose(test_pred));
%% Confusion matrix and Classification matrix for least squares
train_ConfMat = confusionmat(train_labels_extra_original,train_pred)
% Create classification matrix (rows should sum to 1) 
t1 = countcats(train_labels_extra_original);
train_ClassMat = train_ConfMat(1:2,1:2)./(meshgrid(t1(1:2))')
% mean group accuracy and std
train_acc_least = mean(diag(train_ClassMat))
train_std_least = std(diag(train_ClassMat))

test_ConfMat = confusionmat(test_labels_extra_original,test_pred)
% Create classification matrix (rows should sum to 1)
t2 = countcats(test_labels_extra_original);
test_ClassMat = test_ConfMat(1:2,1:2)./(meshgrid(t2(1:2))')
% mean group accuracy and std
test_acc_least = mean(diag(test_ClassMat));
test_std_least = std(diag(test_ClassMat));
%% Fisher Projection
% obtaining fischer coefficients 
w_fischer = fischer_projection(train_featureVector_extra_original,train_labels_extra);

% obtaining fischer projection for training data and test data
train_projected = train_featureVector_extra_original * w_fischer;
test_projected = test_featureVector_extra_original * w_fischer;
%% Fischer Discriminant by KNN

%KNN classifier for the training data
[closest_neighbours distance] = fischer_discriminant(train_projected,train_projected,10);
closest_neighbours = mapping_to_classes_fischer(closest_neighbours,train_labels_original);
fischer_train_labels = categorical(transpose(closest_neighbours));

%KNN classifier for the test data
[closest_neighbours_test distance_test] = fischer_discriminant(train_projected,test_projected,10);
closest_neighbours_test = mapping_to_classes_fischer(closest_neighbours_test,train_labels_extra_original);
fischer_test_labels = categorical(transpose(closest_neighbours_test)); 

%% Confusion matrix and classification matrix

train_ConfMat = confusionmat(train_labels_extra_original,fischer_train_labels)
% Create classification matrix (rows should sum to 1)
train_ClassMat = train_ConfMat(1:2,1:2)./(meshgrid(t1(1:2))')
% mean group accuracy and std
train_acc = mean(diag(train_ClassMat))
train_std = std(diag(train_ClassMat))

% Create confusion matrix
 test_ConfMat = confusionmat(test_labels_extra_original,fischer_test_labels)
% Create classification matrix (rows should sum to 1)
 test_ClassMat = test_ConfMat(1:2,1:2)./(meshgrid(t1(1:2))')
% mean group accuracy and std
 test_acc = mean(diag(test_ClassMat))
 test_std = std(diag(test_ClassMat))



